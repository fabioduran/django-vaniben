from django.contrib import admin
from apps1.models import Personas


# Register your models here.
class AdminPersona(admin.ModelAdmin):

    list_display = ('id', 'nombre', 'apellido', 'edad', )
    list_filter = ('apellido',)
    search_fields = ('nombre', 'apellido', )


admin.site.register(Personas, AdminPersona)
