from django.conf.urls import url
from .views import (test,
                    agregar_usuario,
                    save_agregar_usuario,
                    editar_usuario,
                    editar_usuario_guardar,
                    delete_usuario,
                    )


urlpatterns = [
    url(r'^personas$', test, name='personas'),
    url(r'^personas/add$', agregar_usuario, name='nuevo-usuario'),
    url(r'^personas/edit/(?P<id>\d+)$', editar_usuario, name='editar-usuario'),
    url(r'^personas/delete$', delete_usuario, name='delete-usuario'),
    url(r'^personas/edit/save$', editar_usuario_guardar, name='editar-usuario-guardar'),
    url(r'^personas/add/save$', save_agregar_usuario, name='save-nuevo-usuario'),
]
