from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse
from apps1.models import Personas
from django.db.models import Q
from django.shortcuts import redirect
# Create your views here.


def test(request):

    if request.method == 'GET':
        template = loader.get_template('index.html')
        context = {}
        p = Personas.objects.all().order_by('nombre', 'apellido')
        context['Personas'] = p
    else:
        busqueda = request.POST.get("busqueda")
        p = Personas.objects.all().filter(Q(nombre__istartswith=busqueda) | Q(apellido__istartswith=busqueda))
        context = {}
        context['Personas'] = p
        template = loader.get_template('index.html')
    return HttpResponse(template.render(context, request))


def agregar_usuario(request):
    template = loader.get_template('nuevo-usuario.html')
    context = {}
    return HttpResponse(template.render(context, request))


def save_agregar_usuario(request):
    if request.method == 'POST':
        nombre_form = request.POST.get("nombre")
        apellido_form = request.POST.get("apellido")
        edad_form = request.POST.get("edad")

        if nombre_form != '' or apellido_form != '':
            p = Personas(nombre=nombre_form,
                     apellido=apellido_form,
                     edad=edad_form)

            p.save()

    return redirect("/personas")


def editar_usuario(request, *args, **kwargs):

    template = loader.get_template('nuevo-usuario.html')
    _id_persona = kwargs['id']
    query = Personas.objects.all().filter(id=_id_persona)
    query = query.first()
    context = {}
    context['nombre'] = query.nombre
    context['apellido'] = query.apellido
    context['edad'] = query.edad
    context['id'] = _id_persona
    context['editar'] = True
    context['title'] = "Actualiza"

    return HttpResponse(template.render(context, request))


def editar_usuario_guardar(request, *args, **kwargs):

    if request.method == 'POST':
        nombre = request.POST.get("nombre")
        apellido = request.POST.get("apellido")
        edad = request.POST.get("edad")
        _id_persona = request.POST.get("id")
        query = Personas.objects.all().filter(id=_id_persona)
        query = query.first()
        query.nombre = nombre
        query.apellido = apellido
        query.edad = edad
        query.save()

        return redirect("/personas")


def delete_usuario(request):
    if request.method == "GET":
        _id = request.GET.get("id")
        query = Personas.objects.all().filter(id=_id)
        query.delete()

        return redirect("/personas")
