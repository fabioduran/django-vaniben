from django.db import models

# Create your models here.


class Personas(models.Model):
    nombre = models.CharField(max_length=50, blank=False, null=False)
    apellido = models.CharField(max_length=50, blank=True, null=True)
    edad = models.IntegerField(default=0)

    def __str__(self):
        return self.nombre + ' ' + self.apellido

    class Meta:
        verbose_name_plural = 'Personas'
